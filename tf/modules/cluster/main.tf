locals {
  app = "nodezoo"
}

resource "aws_security_group" "lb" {
  name = "${local.app}-lb-sg"
  vpc_id = var.nodezoo_vpc_id
  ingress {
    protocol = "tcp"
    from_port = 443
    to_port = 443
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol = "tcp"
    from_port = 80
    to_port = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol = "-1"
    from_port = 0
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "${local.app}-lb-sg"
  }
}

resource "aws_security_group" "ecs_tasks" {
  vpc_id = var.nodezoo_vpc_id
  ingress {
    protocol = "tcp"
    from_port = 8080
    to_port = 8080
    cidr_blocks = ["0.0.0.0/0"]
  }

  ingress {
    protocol = "tcp"
    from_port = 80
    to_port = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol = "-1"
    from_port = 0
    to_port = 0
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = {
    Name = "${local.app}-ecs-tasks-sg"
  }
}

output "nodezoo_sg_ecs_tasks_id" {
  value = aws_security_group.ecs_tasks.id
}

data "aws_availability_zones" "available" {
  state = "available"
}

resource "aws_lb" "lb" {
  name = "${local.app}-lb"
  load_balancer_type = "network"
  internal = false
  security_groups = [aws_security_group.lb.id]
  subnets = [
    var.nodezoo_public_1_subnet_id,
    var.nodezoo_public_2_subnet_id
  ]
  access_logs {
    bucket = var.nodezoo_tf_bucket_id
    prefix = "${local.app}-lb"
    enabled = true
  }
}

resource "aws_lb_target_group" "main" {
  name = "${local.app}-info-service-tg"
  port = 8080
  protocol = "TCP"
  vpc_id = var.nodezoo_vpc_id
  target_type = "ip"
}

output "nodezoo_info_service_tg_arn" {
  value = aws_lb_target_group.main.arn
}

resource "aws_lb_listener" "http" {
  load_balancer_arn = aws_lb.lb.arn
  port = 80
  protocol = "TCP"
  default_action {
    type = "forward"
    target_group_arn = aws_lb_target_group.main.arn
  }
}

resource "aws_service_discovery_http_namespace" "nodezoo" {
  name = "nodezoo"
}

output "discovery_http_namespace_arn" {
  value = aws_service_discovery_http_namespace.nodezoo.arn
}

resource "aws_ecs_cluster" "nodezoo" {
  name = "${local.app}-cluster"
  service_connect_defaults {
    namespace = aws_service_discovery_http_namespace.nodezoo.arn
  }
}

output "nodezoo_ecs_cluster_id" {
  value = aws_ecs_cluster.nodezoo.id
}

resource "aws_ecs_cluster_capacity_providers" "nodezoo" {
  cluster_name = aws_ecs_cluster.nodezoo.name
  capacity_providers = ["FARGATE"]
}

resource "aws_iam_role" "ecs_task_execution_role" {
  name = "${local.app}-ecsTaskExecutionRole"
  assume_role_policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Action": "sts:AssumeRole",
      "Principal": {
        "Service": "ecs-tasks.amazonaws.com"
      },
     "Effect": "Allow",
     "Sid": ""
    }
  ]
}
EOF
}

resource "aws_iam_role_policy_attachment" "ecs-task-execution-role-policy-attachment" {
  role = aws_iam_role.ecs_task_execution_role.name
  policy_arn = "arn:aws:iam::aws:policy/service-role/AmazonECSTaskExecutionRolePolicy"
}

output "ecs_task_execution_role_arn" {
  value = aws_iam_role.ecs_task_execution_role.arn
}

output "nodezoo_ecs_host" {
  value = aws_lb.lb.dns_name
}
