variable "nodezoo_vpc_id" {
  type = string
  description = "The ID of the VPC to use for cluster elements"
}

variable "nodezoo_subnet_private_1_id" {
  type = string
  description = "The ID of the first subnet for the ALB"
}

variable "nodezoo_subnet_private_2_id" {
  type = string
  description = "The ID of the second subnet for the ALB"
}

variable "nodezoo_tf_bucket_id" {
  type = string
  description = "The ID of Nodezoo TF bucket"
}

variable "nodezoo_public_1_subnet_id" {
  type = string
  description = "The ID of one Nodezoo public subnet"
}

variable "nodezoo_public_2_subnet_id" {
  type = string
  description = "The ID of another public subnet"
}
