variable "nodezoo_vpc_id" {
  type = string
  description = "The ID of the VPC to use for network elements"
}
