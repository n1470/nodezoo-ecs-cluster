locals {
  app = "nodezoo"
}

resource "aws_internet_gateway" "nodezoo_igw" {
  vpc_id = var.nodezoo_vpc_id
  tags = {
    Name = local.app
  }
}

resource "aws_eip" "nodezoo_1" {
  domain = "vpc"
  depends_on = [aws_internet_gateway.nodezoo_igw]
}

resource "aws_eip" "nodezoo_2" {
  domain = "vpc"
  depends_on = [aws_internet_gateway.nodezoo_igw]
}

data "aws_availability_zones" "available" {
  state = "available"
}

resource "aws_subnet" "nodezoo_public_1" {
  vpc_id = var.nodezoo_vpc_id
  cidr_block = "10.0.1.0/24"
  availability_zone = data.aws_availability_zones.available.names[0]
  tags = {
    Name = "${local.app}_public_1"
  }
}

output "nodezoo_public_1_subnet_id" {
  value = aws_subnet.nodezoo_public_1.id
}

resource "aws_subnet" "nodezoo_private_1" {
  vpc_id = var.nodezoo_vpc_id
  cidr_block = "10.0.10.0/24"
  availability_zone = data.aws_availability_zones.available.names[0]
  tags = {
    Name = "${local.app}_private_1"
  }
}

output "nodezoo_subnet_private_1_id" {
  value = aws_subnet.nodezoo_private_1.id
}

resource "aws_nat_gateway" "nat_gateway_1" {
  subnet_id = aws_subnet.nodezoo_public_1.id
  allocation_id = aws_eip.nodezoo_1.allocation_id
  depends_on = [aws_internet_gateway.nodezoo_igw]
}

resource "aws_subnet" "nodezoo_public_2" {
  vpc_id = var.nodezoo_vpc_id
  cidr_block = "10.0.2.0/24"
  availability_zone = data.aws_availability_zones.available.names[1]
  tags = {
    Name = "${local.app}_public_2"
  }
}

output "nodezoo_public_2_subnet_id" {
  value = aws_subnet.nodezoo_public_2.id
}

resource "aws_subnet" "nodezoo_private_2" {
  vpc_id = var.nodezoo_vpc_id
  cidr_block = "10.0.20.0/24"
  availability_zone = data.aws_availability_zones.available.names[1]
  tags = {
    Name = "${local.app}_private_2"
  }
}

output "nodezoo_subnet_private_2_id" {
  value = aws_subnet.nodezoo_private_2.id
}

resource "aws_nat_gateway" "nat_gateway_2" {
  subnet_id = aws_subnet.nodezoo_public_2.id
  allocation_id = aws_eip.nodezoo_2.allocation_id
  depends_on = [aws_internet_gateway.nodezoo_igw]
}

resource "aws_route_table" "nodezoo_rtb_public" {
  vpc_id = var.nodezoo_vpc_id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.nodezoo_igw.id
  }

  depends_on = [aws_internet_gateway.nodezoo_igw]
}

resource "aws_route_table_association" "nodezoo_rtb_public_1" {
  route_table_id = aws_route_table.nodezoo_rtb_public.id
  subnet_id = aws_subnet.nodezoo_public_1.id
}

resource "aws_route_table_association" "nodezoo_rtb_public_2" {
  route_table_id = aws_route_table.nodezoo_rtb_public.id
  subnet_id = aws_subnet.nodezoo_public_2.id
}

resource "aws_route_table" "nodezoo_rtb_private_1" {
  vpc_id = var.nodezoo_vpc_id
  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.nat_gateway_1.id
  }

  depends_on = [aws_nat_gateway.nat_gateway_1]
}

resource "aws_route_table_association" "nodezoo_rtb_private_1" {
  route_table_id = aws_route_table.nodezoo_rtb_private_1.id
  subnet_id = aws_subnet.nodezoo_private_1.id
}

resource "aws_route_table" "nodezoo_rtb_private_2" {
  vpc_id = var.nodezoo_vpc_id
  route {
    cidr_block = "0.0.0.0/0"
    nat_gateway_id = aws_nat_gateway.nat_gateway_2.id
  }

  depends_on = [aws_nat_gateway.nat_gateway_2]
}

resource "aws_route_table_association" "nodezoo_rtb_private_2" {
  route_table_id = aws_route_table.nodezoo_rtb_private_2.id
  subnet_id = aws_subnet.nodezoo_private_2.id
}
