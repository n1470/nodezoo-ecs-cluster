locals {
  app = "nodezoo"
  environment = "staging"
  region = "ap-south-1"
}

terraform {
  cloud {
    organization = "Sayantam-Learning"
    workspaces {
      name = "info-system-staging-ap-south1"
    }
  }
}

provider "aws" {
  region = local.region
  profile = var.aws_profile
}

module "s3_bucket_for_logs" {
  source = "terraform-aws-modules/s3-bucket/aws"
  bucket = "${local.app}-tf-${local.environment}"
  acl    = "log-delivery-write"
  force_destroy = true
  control_object_ownership = true
  object_ownership         = "ObjectWriter"

  attach_elb_log_delivery_policy = true
  attach_lb_log_delivery_policy = true
}

resource "aws_vpc" "nodezoo" {
  cidr_block = "10.0.0.0/16"
  enable_dns_support = true
  enable_dns_hostnames = true

  tags = {
    Name = "${local.app}-${local.environment}-${local.region}"
    Bucket = module.s3_bucket_for_logs.s3_bucket_arn
  }
}

module "networking" {
  source = "../modules/networking"
  nodezoo_vpc_id = aws_vpc.nodezoo.id
}

module "cluster" {
  source = "../modules/cluster"
  nodezoo_tf_bucket_id = module.s3_bucket_for_logs.s3_bucket_id
  nodezoo_vpc_id = aws_vpc.nodezoo.id
  nodezoo_subnet_private_1_id = module.networking.nodezoo_subnet_private_1_id
  nodezoo_subnet_private_2_id = module.networking.nodezoo_subnet_private_2_id
  nodezoo_public_1_subnet_id = module.networking.nodezoo_public_1_subnet_id
  nodezoo_public_2_subnet_id = module.networking.nodezoo_public_2_subnet_id
}

resource "aws_cloudwatch_log_group" "nodezoo" {
  name = local.app
  tags = {
    Environment = local.environment
    Application = "${local.app}-cluster"
  }
}

output "nodezoo_subnet_private_1_id" {
  value = module.networking.nodezoo_subnet_private_1_id
}

output "nodezoo_subnet_private_2_id" {
  value = module.networking.nodezoo_subnet_private_2_id
}

output "nodezoo_sg_ecs_tasks_id" {
  value = module.cluster.nodezoo_sg_ecs_tasks_id
}

output "nodezoo_ecs_cluster_id" {
  value = module.cluster.nodezoo_ecs_cluster_id
}

output "ecs_task_execution_role_arn" {
  value = module.cluster.ecs_task_execution_role_arn
}

output "nodezoo_ecs_host" {
  value = module.cluster.nodezoo_ecs_host
  description = "The hostname for the Nodezoo Info System"
}

output "nodezoo_info_service_tg_arn" {
  value = module.cluster.nodezoo_info_service_tg_arn
}

output "discovery_http_namespace_arn" {
  value = module.cluster.discovery_http_namespace_arn
}
